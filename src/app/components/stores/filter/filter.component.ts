import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'nc-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  @Output()
  filterEmpty = new EventEmitter<boolean>();

  displayEmpty = true;

  constructor() { }

  ngOnInit(): void {
  }

  toggle(): void {
    this.displayEmpty = !this.displayEmpty;
    this.filterEmpty.emit(this.displayEmpty);
  }
}
