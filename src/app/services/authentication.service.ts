import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  /**
   * Créer un compte utilisateur
   * @param email   Identifiant de l'utilisateur
   * @param name    Nom, prénom ou pseudo
   * @param password  Mot de passe
   */
  register(email: string, name: string, password: string): Observable<User> {
    const body = {email, name, password};
    return this.http.post<User>(`${environment.baseURL}/register`, body);
  }

  /**
   * S'authentifier sur l'application
   * @param email     Identifiant de l'utilisateur
   * @param password  Mot de passe
   */
  authenticate(email: string, password: string): Observable<User> {
    const body = {email, password};
    return this.http.post<User>(`${environment.baseURL}/auth`, body);
  }
}
